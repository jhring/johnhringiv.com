# Project Name

This project is a web application built using PHP and JavaScript. It is configured to run with Nginx and PHP-FPM inside a Docker container.

## Table of Contents

- [Project Structure](#project-structure)
- [Usage](#usage)
- [Configuration](#configuration)

## Project Structure
```
.
├── config
│   ├── conf.d
│   │   └── default.conf
│   ├── fpm-pool.conf
│   ├── nginx.conf
│   ├── php.ini
│   └── supervisord.conf
├── www
│   └── index.php
│   └── *.php
│   └── css
│   └── js
│   └── img
├── Dockerfile
└── .gitattributes
```

## Usage

1. Run the Docker container:
    ```sh
    docker run -p 8080:8080 your-image-name
    ```

2. Access the application in your web browser at `http://localhost:8080`.

### Build for another host
```sh
docker build -t johnhringiv.com:latest .
docker save -o myimage johnhringiv.com:latest
```

## Configuration

- **Nginx Configuration**: Located in `config/nginx.conf` and `config/conf.d/default.conf`.
- **PHP-FPM Configuration**: Located in `config/fpm-pool.conf` and `config/php.ini`.
- **Supervisor Configuration**: Located in `config/supervisord.conf`.