<div class="copyright mt-4">
    <div class="headline mb-3">
        <h2>Affiliated Organizations</h2>
    </div>
    <div class="container">
        <div class="row d-flex flex-wrap align-items-center">
            <div class="col-3">
                <a href="https://www.threatstack.com/">
                    <img class="img-fluid" src="img/affilated/threat-stack-logo.svg" alt="Threat Stack Logo">
                </a>
            </div>
            <div class="col-3">
                <a href="http://vermontcomplexsystems.org/">
                    <img class="img-fluid" src="img/affilated/roboctopus.png" alt="Vermont Complex Systems Center Logo">
                </a>
            </div>
            <div class="col-3">
                <a href="https://www.uvm.edu/">
                    <img class="img-fluid" src="img/affilated/commonground.png" alt="UVM Logo">
                </a>
            </div>
            <div class="col-3">
                <a href="https://www.mitre.org/">
                    <img class="img-fluid" src="img/affilated/Mitre_Corporation_logo.png" alt="MITRE Logo">
                </a>
            </div>
        </div>
    </div>

    <div class="container">
        <p class="text-center"><small>Copyright ©2020-2022 John H. Ring IV. All rights reserved.</small></p>
    </div>
</div><!--/copyright-->

</div><!--/container-->

<!-- External JS -->
<!-- todo check poppler -->
<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

<script src="js/image_modal.js"></script>

</body>
</html>
