<?php
include "util.php";

$phpSelf = htmlentities($_SERVER['PHP_SELF'], ENT_QUOTES, "UTF-8");
$fname = pathinfo($phpSelf)['filename'];
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!--  favicon  -->
    <link rel="apple-touch-icon" sizes="180x180" href="img/favicon_io/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon_io/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/favicon_io/favicon-16x16.png">
    <link rel="manifest" href="img/favicon_io/site.webmanifest">

    <!-- External CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/5ca6deeea9.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/academicons/1.9.1/css/academicons.min.css" integrity="sha512-b1ASx0WHgVFL5ZQhTgiPWX+68KjS38Jk87jg7pe+qC7q9YkEtFq0z7xCglv7qGIs/68d3mAp+StfC8WKC5SSAg==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <link href="css/custom.css" rel="stylesheet">

    <title>John Ring's Webpage</title>
</head>
<body id="<?php echo $fname; ?>" class="bg-light">
<nav class="navbar navbar-default navbar-expand-lg navbar-dark bg-dark navbar-static-top">
    <div class="container-fluid col-12">
        <div class="col mx-4">
            <a class="navbar-brand" href="index.php">John H. Ring IV</a>
            <button type="button" data-bs-target="#navbarCollapse" data-bs-toggle="collapse" class="navbar-toggler ml-auto">
                <span class="navbar-toggler-icon"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <!-- Collection of nav links, forms, and other content for toggling -->
        <div id="navbarCollapse" class="collapse navbar-collapse justify-content-start">
            <ul class="nav navbar-nav col">
                <?php
                $pages = array("index", "about", "research", "press");
                foreach($pages as &$page) {
                    $name = $page == "index" ? "Home" : ucfirst($page);
                    $active = $fname == $page ? " active" : "";
                    echo "<li class='nav-item'><a class='nav-link{$active}' href='{$page}.php'>{$name}</a></li>";
                }
                ?>
            </ul>
            <ul class="nav navbar-nav navbar-right ml-auto flex-row social-network social-circle">
                <li class="nav-item">
                    <a href="https://gitlab.com/users/jhring/projects" class="nav-link icoGitlab"><i class="fa fa-gitlab"></i></a>
                </li>
                <li class="nav-item">
                    <a href="https://twitter.com/johnhringiv" class="nav-link icoTwitter"><i class="fa fa-twitter"></i></a>
                </li>
                <li class="nav-item">
                    <a href="https://www.instagram.com/johnhringiv/" class="nav-link icoInstagram"><i class="fa fa-instagram"></i></a>
                </li>
                <li class="nav-item">
                    <a href="https://www.linkedin.com/in/johnhringiv" class="nav-link icoLinkedin"><i class="fa fa-linkedin"></i></a>
                </li>
                <li class="nav-item">
                    <a href="https://scholar.google.com/citations?user=ubl_nT8AAAAJ&hl=en" class="nav-link icoScholar"><i class="ai ai-google-scholar"></i></a>
                </li>
            </ul>
        </div>
    </div>

</nav>
<div class="container mt-4 col-9">