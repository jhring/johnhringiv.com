<?php
include "top.php";
?>
<div class="row">
    <figure class="figure text-center col">
        <img src="img/self_pic.jpg"
             alt="pictured John H. Ring IV"
             class="figure-img img-fluid rounded-circle mw-50"
             style="max-height: 400px;"
        >
        <figcaption class="figure-caption">
            PhD Candidate in Computer Science<br/>
            <b>Center for Computer Security and Data Privacy</b>, University of Vermont
            <br/>
            <i>Research Interests: Computer Security, Computational Finance</i>
            <br/>
            <a href="mailto:jhring@uvm.ed" class="unlink"><i class="fa fa-envelope-square"></i> jhring@uvm.edu</a>
            &middot
            <i class="fa fa-phone"> (716) 418 - 4142</i>
        </figcaption>
    </figure>
</div>


<?php
include "footer.php";
?>