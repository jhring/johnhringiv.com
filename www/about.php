<?php
include "top.php";
?>
<div class="container row vertical-align mt-4">
    <div class="col-md-6">
        <img src="img/self_pic.jpg" alt="" class="img-rounded mw-100">
    </div>
    <div class="col-md-6 hidden-sm hidden-xs">
        <h1>A Bit About Me</h1>
        <p>I am pursing a Ph.D in Computer science at the University of Vermont
            where I previously received a B.S in Computer Science. Much of my day
            is spent contemplating my preferred research topics:
            complex systems, computer security and finance. In my
            spare time I am found either out on the water sailing or looking into a computer
            terminal trying to figure out what I just broke.</p>
        <br>
        My resume may be viewed <a href="PDF/Ring_Resume.pdf">here</a>
    </div>
</div>
<?php
include "footer.php";
?>
