<?php
include "top.php";
?>

<div class="container">
    <div class="headline mb-3">
        <h2><a class="unlink" href="https://www.uvm.edu/uvmnews/news/tiny-price-gaps-cost-investors-billions">Tiny Price Gaps Cost Investors Billions</a></h2>
    </div>
    <p><small>January 2020, by UVM Today</small></p>
    <div class="row">
        <figure class="col-md-6 image-modal-content">
            <img src="img/press/briantivnan-800x400_0.jpg" alt="Brian Tivan, Colin Van Oort, and John Ring" class="img-fluid">
            <figcaption class="figure-caption">
                UVM Computational Finance Lab founder and lead scientist Brian Tivnan (left) along with founding members
                Colin Van Oort and John H. Ring IV. <small>(Photo: Josh Brown)</small>
            </figcaption>
        </figure>
        <div class="col-md-6 float-left">
            <p>A team from the Computational Finance Lab at UVM made a first-of-its-kind comprehensive study of the U.S.
                stock market. They found billions of opportunities over the course of one year for some traders to get
                price information sooner than others allowing so called high frequency traders to buy stocks at
                slightly better prices, and then, in far less than the blink of an eye, turn around and sell them at a
                profit.</p>
        </div>
    </div>
</div>

<div class="container">
    <div class="headline mb-3">
        <h2>
            <a class="unlink" href="https://www.uvm.edu/uvmnews/news/uvm-enters-research-partnership-threat-stack-leading-cloud-based-cybersecurity-firm">
                UVM Enters Research Partnership with Threat Stack
            </a>
        </h2>
    </div>
    <p><small>December 2019, by UVM Today</small></p>
    <div class="row">
        <figure class="col-md-6 image-modal-content">
            <img src="img/press/threatstack800x400.jpg" alt="John Ring with ThreatStack employees" class="img-fluid">
            <figcaption class="figure-caption">
                John Ring, center, with members of Threat Stack’s Security Operations team at the company’s Boston headquarters.
                Ring, a doctoral candidate in UVM’s Computer Science department, was embedded at the company for two weeks this fall.
            </figcaption>
        </figure>
        <div class="col-md-6 float-left">
            <p>This fall Threat Stack and UVM began a relationship with the potential to offer significant benefits to
                both the company and the university. Skalka, fellow Computer Science faculty member Joe Near and
                doctoral candidate John Ring have begun a project designed to enhance Threat Stack’s threat assessment
                process with artificial intelligence that could make cloud-based cybersecurity more efficient and
                accurate and lengthen the company’s lead in the marketplace. </p>
        </div>
    </div>
</div>

<div class="container">
    <div class="headline mb-3">
        <h2><a class="unlink" href="https://www.wsj.com/articles/brief-price-gaps-in-stocks-cost-investors-2-billion-a-year-11550152800">Brief Price Gaps in Stocks Cost Investors $2 Billion a Year</a></h2>
    </div>
    <p><small>Febuary 2019, by The Wall Street Journal</small></p>
    <div class="row">
        <figure class="col-md-6 image-modal-content">
            <img src="img/press/in_a_flash.jpg" alt="WSJ Dislocation Example" class="img-fluid zoom" style="max-height: 525px">
        </figure>
        <div class="col-md-6 float-left">
            <p>A federally funded study provides new evidence of momentary pricing discrepancies that researchers say
                can be exploited by high-speed traders looking to make a quick profit.</p>
        </div>
    </div>
</div>
<!-- modal popup (displayed none by default) -->
<div class="image-modal-popup">
    <div class="wrapper">
        <span>&times;</span>
        <img src="" alt="Image Modal">
        <div class="description">
            <h1></h1>
            <p></p>
        </div>
    </div>
</div>

<?php
include "footer.php";
?>
