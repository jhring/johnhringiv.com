<?php
function get_conn() {
    $cred_arr = parse_ini_file("project.ini");
    $conn = mysqli_connect($cred_arr["host"], $cred_arr["user"], $cred_arr["pass"], $cred_arr["name"]);
    if (!$conn) {
        echo("error connection failed");
    }
    return $conn;
}
